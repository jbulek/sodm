from cx_Freeze import setup, Executable

company_name = "Divisionary"
product_name = "sodm"

options = {
    'build_exe': {
        'includes': ['numpy', 'pyfits', 'PIL', 'cv2', 'pylab', 'matplotlib.backends.backend_tkagg', "PIL.Image",
                     'FileDialog'],
        'optimize': 2,
        'include_files': ['modules/', 'app/'],
        'excludes': ['boto.compat.sys',
                     'boto.compat._sre',
                     'boto.compat._json',
                     'boto.compat._locale',
                     'boto.compat._struct',
                     'boto.compat.array',
                     'collections.abc',
                     'collections.sys'],
    },
    'bdist_msi': {
        'upgrade_code': '{66620F3A-DC3A-11E2-B341-002219E9B01E}',
        'add_to_path': False,
        'initial_target_dir': r'[ProgramFilesFolder]\%s\%s' % (company_name, product_name),
    },
}

setup(
    name="sodm",
    version="1.0",
    description=" ",
    options=options,
    executables=[Executable(script="main.py", targetName="sodm.exe", icon="app\sun.ico")],
    #executables=[Executable(script="main.py", base="Win32GUI", targetName="sodm.exe", icon="app\sun.ico")],
)
