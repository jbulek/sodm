import pygtk
pygtk.require('2.0')
import gtk

class Window(gtk.Window):
    def __init__(self):
        builder = gtk.Builder()
        builder.add_from_file("app/main.gui")
        builder.connect_signals(self)
        window = builder.get_object("mainMenuWindow")
        window.show_all()

    def mmCalibButton_clicked_cb(self, *args):
        execfile('modules/calibration/main.py')

    def mmFtbButton_clicked_cb(self, *args):
        execfile('modules/the_best/main.py')

    def mmLdButton_clicked_cb(self, *args):
        execfile('modules/limb_darkening/main.py')

    def onDeleteWindow(self, *args):
        gtk.main_quit(*args)
        return 0

if __name__ == '__main__':
    window = Window()
    gtk.main()
