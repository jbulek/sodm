import FileDialog
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import RectangleSelector, Button
import pyfits

x1 = None
x2 = 0
y1 = 0
y2 = 0

confirmation = False

def lingray(x, a=None, b=None):
    """
    Auxiliary function that specifies the linear gray scale.
    a and b are the cutoffs : if not specified, min and max are used
    """
    if a == None:
        a = np.min(x)
    if b == None:
        b = np.max(x)
    return 255.0 * (x - float(a)) / (b - a)


def line_select_callback(eclick, erelease):
    global x1, y1, x2, y2
    x1, y1 = eclick.xdata, eclick.ydata
    x2, y2 = erelease.xdata, erelease.ydata


def toggle_selector(event):
    print(' Key pressed.')
    if event.key in ['Q', 'q'] and toggle_selector.RS.active:
        print(' RectangleSelector deactivated.')
        # toggle_selector.RS.set_active(False)
    if event.key in ['A', 'a'] and not toggle_selector.RS.active:
        print(' RectangleSelector activated.')
        toggle_selector.RS.set_active(True)

def confirm(e):
    global confirmation
    confirmation = True
    plt.close("all")

def get_xy(r):
    global confirmation
    confirmation = False
    fig, current_ax = plt.subplots()

    image = pyfits.getdata(r[0])
    new_image_data = lingray(image)
    new_image_min = 0.
    new_image_max = np.max(new_image_data)
    toggle_selector.RS = RectangleSelector(current_ax, line_select_callback,
                                           drawtype='box', useblit=True,
                                           button=[1, 3],  # don't use middle button
                                           minspanx=5, minspany=5,
                                           spancoords='pixels',
                                           interactive=True)
    plt.imshow(new_image_data, vmin=new_image_min,
               vmax=new_image_max, cmap='gray')
    axcut = plt.axes([0.9, 0.0, 0.1, 0.075])
    bcut = Button(axcut, 'OK', color='green')
    bcut.on_clicked(confirm)
    plt.show()
    # plt.show(block=False)
    plt.close("all")
    if confirmation:
        if x1==None:
            return None
        else:
            return [int(x1), int(y1), int(x2), int(y2)]
    else:
        return None
