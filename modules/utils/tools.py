import os
import fnmatch
import pyfits
import numpy as np
from PIL import Image


def convert(input, output, coords=None):
    folder = input[0].split("\\")[:-1]
    folder = '/'.join(folder)
    folder += '/'
    if not os.path.exists(folder + 'tmp'):
        os.makedirs(folder + 'tmp')
    print("[*] Converting FITS to TIFF")
    data_inputs = fnmatch.filter(os.listdir(folder + '/'), '*.*')
    for i in range(len(data_inputs)):
        if coords == None:
            image = pyfits.getdata(folder + '/' + data_inputs[i])
        else:
            image = pyfits.getdata(folder + '/' + data_inputs[i])[coords[1]:coords[3], coords[0]:coords[2]]
        img = Image.fromarray(image.astype(np.uint16))
        img.save(folder + 'tmp/' + data_inputs[i] + "." + output)
