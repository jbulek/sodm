from __future__ import division
import gtk
import pygtk

pygtk.require('2.0')
gtk.gdk.threads_init()


class Window(gtk.Window):
    def __init__(self):
        builder = gtk.Builder()
        builder.add_from_file("modules/calibration/main.gui")
        builder.connect_signals(self)
        # GUI objects
        self.calibrateButton = builder.get_object('calibrateButton')
        self.findthebestButton = builder.get_object('findthebestButton')
        self.fileDialog = builder.get_object('fileDialog')
        self.fileChooser = builder.get_object('filechooser')
        self.rawTreeView = builder.get_object('rawTreeView')
        self.darkTreeView = builder.get_object('darkTreeView')
        self.flatTreeView = builder.get_object('flatTreeView')
        self.progressbar = builder.get_object('progressbar')
        self.dfTreeView = builder.get_object('dfTreeView')
        self.rawCountLabel = builder.get_object('rawCountLabel')
        self.darkCountLabel = builder.get_object('darkCountLabel')
        self.flatCountLabel = builder.get_object('flatCountLabel')
        self.dfCountLabel = builder.get_object('dfCountLabel')
        self.infoLabel = builder.get_object('infoLabel')
        # Global variables
        self.fileList = {"raw": [], "dark": None, "flat": None, "darkflat": None}
        self.currentList = ""
        self.r_store = gtk.ListStore(str)
        self.d_store = gtk.ListStore(str)
        self.f_store = gtk.ListStore(str)
        self.df_store = gtk.ListStore(str)
        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn("", renderer, text=0)
        self.rawTreeView.append_column(column)
        column = gtk.TreeViewColumn("", renderer, text=0)
        self.darkTreeView.append_column(column)
        column = gtk.TreeViewColumn("", renderer, text=0)
        self.flatTreeView.append_column(column)
        column = gtk.TreeViewColumn("", renderer, text=0)
        self.dfTreeView.append_column(column)
        window = builder.get_object("calibrationWindow")
        window.show_all()

    def rawChooseButtonClicked(self, *args):
        self.currentList = "raw"
        self.fileDialog.run()

    def darkChooseButtonClicked(self, *args):
        self.currentList = "dark"
        self.fileDialog.run()

    def flatChooseButtonClicked(self, *args):
        self.currentList = "flat"
        self.fileDialog.run()

    def dfChooseButtonClicked(self, *args):
        self.currentList = "darkflat"
        self.fileDialog.run()

    def fileDialogOKButton_clicked(self, *args):
        self.fileList[self.currentList] = self.fileChooser.get_filenames()
        if self.currentList == 'raw':
            self.r_store.clear()
            for file in self.fileList['raw']:
                self.r_store.append([file.split("\\")[-1]])
            self.rawTreeView.set_model(self.r_store)
            self.rawCountLabel.set_text(str(len(self.fileList[self.currentList])))
            self.calibrateButton.set_sensitive(True)
        elif self.currentList == 'dark':
            self.d_store.clear()
            for file in self.fileList['dark']:
                self.d_store.append([file.split("\\")[-1]])
            self.darkTreeView.set_model(self.d_store)
            self.darkCountLabel.set_text(str(len(self.fileList[self.currentList])))
        elif self.currentList == 'flat':
            self.f_store.clear()
            for file in self.fileList['flat']:
                self.f_store.append([file.split("\\")[-1]])
            self.flatTreeView.set_model(self.f_store)
            self.flatCountLabel.set_text(str(len(self.fileList[self.currentList])))
        elif self.currentList == 'darkflat':
            self.df_store.clear()
            for file in self.fileList['darkflat']:
                self.df_store.append([file.split("\\")[-1]])
            self.dfTreeView.set_model(self.df_store)
            self.dfCountLabel.set_text(str(len(self.fileList[self.currentList])))
        self.fileDialog.hide()

    def fileDialogCancelButton_clicked_cb(self, *args):
        self.fileDialog.hide()

    def calibrateButtonClicked(self, *args):
        from threading import Thread
        Thread(target=self.calibrate, args=['', self.fileList["raw"], self.fileList["dark"], self.fileList["flat"],
                                            self.fileList["darkflat"]]).start()

    def rawDeleteButton_clicked_cb(self, *args):
        self.r_store.clear()
        self.fileList["raw"] = None

    def darkDeleteButton_clicked_cb(self, *args):
        self.d_store.clear()
        self.fileList["dark"] = None

    def flatDeleteButton_clicked_cb(self, *args):
        self.f_store.clear()
        self.fileList["flat"] = None

    def dfDeleteButton_clicked_cb(self, *args):
        self.df_store.clear()
        self.fileList["darkflat"] = None

    def onDeleteWindow(self, *args):
        gtk.main_quit(*args)
        return 0

    def set_progress_bar_fraction(self, fraction):
        self.progressbar.set_fraction(fraction)

    def calibrate(self, folder, r, d=None, f=None, df=None):
        import os
        import sys
        import numpy as np
        import pyfits
        from PIL import Image
        import gobject
        self.infoLabel.set_text("")
        folder = r[0].split("\\")[:-1]
        folder = '/'.join(folder)
        folder += '/'
        if (d != None):
            print("\n[*] Creating dark frame median image")
            self.infoLabel.set_text("[*] Creating dark frame median image")
            image = []
            dark_files = d
            for i in range(len(dark_files)):
                image.append(pyfits.getdata(dark_files[i]))
            dark_stack = np.array(image)
            dark_median = np.median(dark_stack, axis=0)
            dark_median = dark_median.astype(np.int16)

        if (df != None):
            print("[*] Creating darkflat median image")
            self.infoLabel.set_text("[*] Creating darkflat median image")
            image = []
            darkflat_files = df
            for i in range(len(darkflat_files)):
                image.append(pyfits.getdata(darkflat_files[i]))
            darkflat_stack = np.array(image)
            darkflat_median = np.median(darkflat_stack, axis=0)
            darkflat_median = darkflat_median.astype(np.int16)

        if (f != None):
            image = []
            flat_files = f
            for i in range(len(flat_files)):
                image.append(pyfits.getdata(flat_files[i]))
            if (df != None):
                print("[*] Subtracting darkflat from flat-field")
                self.infoLabel.set_text("[*] Subtracting darkflat from flat-field")
                for i in range(len(image)):
                    image[i] = image[i] - darkflat_median
                    image[i] = image[i].clip(0)
            print("[*] Creating flat-field median image")
            self.infoLabel.set_text("[*] Creating flat-field median image")
            flat_stack = np.array(image)
            flat_median = np.median(flat_stack, axis=0)
            flat_median = flat_median.astype(np.int16)
        image = []
        hdulist = []
        raw_files = r
        raw_file_names = []
        for i in range(len(r)):
            raw_file_names.append(r[i].split("\\")[-1])
        print("[*] Getting headers")
        self.infoLabel.set_text("[*] Getting headers")
        for i in range(len(raw_files)):
            hdulist.append(pyfits.getheader(raw_files[i]))
            image.append(pyfits.getdata(raw_files[i]))
        print("[*] Creating folders")
        self.infoLabel.set_text("[*] Creating folders")
        if not os.path.exists(folder + 'final'):
            os.makedirs(folder + 'final')
        for i in range(len(image)):
            sys.stdout.write("\r[*] Calibrating %d of %d..." % (i + 1, len(image)))
            self.infoLabel.set_text("[*] Calibrating {} of {}...".format(i + 1, len(image)))
            gobject.idle_add(self.set_progress_bar_fraction, (i + 1) / len(image))
            image[i] = image[i].astype(np.int32)
            R = image[i]
            if (d != None):
                im = R - dark_median
            if (f != None):
                im = (im * np.average(flat_median)) / flat_median
            im = im.clip(0, 65536)
            im = im.astype(np.uint16)
            try:
                pyfits.writeto(folder + 'final/' + raw_file_names[i][0:-5] + '_final.fits', im, hdulist[i])
            except:
                e = sys.exc_info()[0]
                self.infoLabel.set_text(str(e))
                return

            img = Image.fromarray(im.astype(np.int16))
        print("\n[!] Calibration done")
        self.infoLabel.set_text("[!] Calibration done")
        img = None
        im = None
        image = None
        hdulist = None


if __name__ == '__main__':
    window = Window()
    gtk.main()
