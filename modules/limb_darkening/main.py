from __future__ import division
import matplotlib
# import PyQt4
# matplotlib.use("qt4agg")
import gtk
import pygtk
from pylab import *

pygtk.require('2.0')
gtk.gdk.threads_init()


class Window(gtk.Window):
    def __init__(self):
        self.builder = gtk.Builder()
        self.builder.add_from_file("modules/limb_darkening/main.gui")
        self.builder.connect_signals(self)
        self.outputFileDialog = self.builder.get_object("fileDialog")
        self.fileChooser = self.builder.get_object("filechooser")
        self.progressbar = self.builder.get_object("progressbar")
        self.doButton = self.builder.get_object("doButton")
        self.infoLabel = self.builder.get_object("infoLabel")
        self.outputFileButton = self.builder.get_object("outputFileButton")
        self.outputFile = ""
        self.thread = None
        window = self.builder.get_object("ldWindow")
        window.show_all()

    def inputFileButton_file_set_cb(self, *args):
        f = self.builder.get_object("inputFileButton").get_filename().split("\\")[-1] + ".tiff"
        self.fileChooser.set_filename(f)
        self.fileChooser.set_current_name(f)
        self.outputFileButton.set_label(f)
        self.outputFile = self.builder.get_object("inputFileButton").get_filename() + ".tiff"

    def outputFileButton_clicked_cb(self, *args):
        self.outputFileDialog.run()

    def fileDialogOKButton_clicked(self, *args):
        self.outputFile = self.fileChooser.get_filename() + ".tiff"
        self.outputFileButton.set_label(self.outputFile)
        self.outputFileDialog.hide()

    def fileDialogCancelButton_clicked_cb(self, *args):
        self.outputFileDialog.hide()

    def doButton_clicked_cb(self, *args):
        from threading import Thread
        input_file = self.builder.get_object("inputFileButton").get_filename()
        coeff = self.builder.get_object("coeficientSpin").get_value()
        r1 = self.builder.get_object("cRadioButton")
        type = [r for r in r1.get_group() if r.get_active()]
        autoxy = self.builder.get_object("autoxyCheckButton").get_active()
        self.thread = Thread(target=self.correct,
                             args=[input_file, self.outputFile, coeff, autoxy, type[0].get_label()])
        self.thread.start()

    def onDeleteWindow(self, *args):
        gtk.main_quit(*args)
        return 0

    def set_progress_bar_fraction(self, fraction):
        self.progressbar.set_fraction(fraction)

    def find_sun(self, sun_image):
        import cv2
        import cv2.cv as cv
        import numpy as np
        img = cv2.imread(sun_image, 0)
        img = cv2.medianBlur(img, 5)
        cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
        circles = cv2.HoughCircles(img, cv.CV_HOUGH_GRADIENT, 4, 1400,
                                   param1=1, param2=1, minRadius=500, maxRadius=600)
        circles = np.uint16(np.around(circles))
        # for i in circles[0, :]:
        #     # draw the outer circle
        #     cv2.circle(cimg, (i[0], i[1]), i[2], (0, 255, 0), 2)
        #     # draw the center of the circle
        #     cv2.circle(cimg, (i[0], i[1]), 2, (0, 0, 255), 3)
        #
        # cv2.imshow('detected circles', cimg)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        return circles

    def get_pixel_value(self, w, theta):
        import numpy as np
        return 2 - (1 - w * (1 - np.cos(np.radians(theta))))

    def normalize(self, im, c, ys, xs):
        from pylab import linspace, meshgrid
        x = linspace(-10, 10, xs)
        y = linspace(-10, 10, ys)
        xx, yy = meshgrid(x, y)
        z = self.get_pixel_value(c, xx ** 2 + yy ** 2)
        z *= im
        return z

    def radial_profile(self, data, center):
        import numpy as np
        y, x = np.indices((data.shape))
        r = np.sqrt((x - center[0]) ** 2 + (y - center[1]) ** 2)
        r = r.astype(np.int)

        tbin = np.bincount(r.ravel(), data.ravel())
        nr = np.bincount(r.ravel())
        radialprofile = tbin / nr
        return radialprofile

    def correct(self, input, output, coef, autoxy, type):
        import gobject
        import numpy as np
        import pyfits
        from PIL import Image
        import sys
        from modules.utils import viewer
        self.doButton.set_sensitive(False)
        gobject.idle_add(self.set_progress_bar_fraction, 0)
        self.infoLabel.set_text("[*] Preparing images")
        print ("[*] Preparing image")
        image = pyfits.getdata(input).astype(np.int32)
        image = np.multiply(image, 7)
        image = image.clip(15000, 65000)
        image = image.astype(np.uint16)
        image = Image.fromarray(image)
        image.save(input + '.tmp.tiff')
        if autoxy is not True:
            self.infoLabel.set_text("[?] Please specify Sun coordinates")
            print ("[?] Please specify Sun coordinates")
            coords = viewer.get_xy([input])
            if coords == None:
                self.infoLabel.set_text("[*] Canceled by user")
                print ("[*] Canceled by user")
                self.doButton.set_sensitive(True)
                self.thread._Thread__stop()
                return 0
        else:
            self.infoLabel.set_text("[*] Getting Sun coordinates")
            print ("[*] Getting Sun coordinates")
            sun = self.find_sun(input + '.tmp.tiff')
            r = [sun[0][0][0], sun[0][0][1], sun[0][0][2]]
            coords = []
            coords.append(r[0] - r[2] - 10)
            coords.append(r[1] - r[2] - 5)
            coords.append(r[0] + r[2] + 3)
            coords.append(r[1] + r[2] + 5)
        y_size = coords[3] - coords[1]
        x_size = coords[2] - coords[0]
        image = pyfits.getdata(input)[coords[1]:coords[3], coords[0]:coords[2]]
        best = [0, 999999]
        if coef == 0:
            self.infoLabel.set_text("[*] Trying to find the best coefficient")
            print ("[*] Trying to find the best coefficient")
            for i in xrange(1, 1000, 5):
                d = i
                sys.stdout.write("\r[*]%d percent done..." % (int(i / 10)))
                gobject.idle_add(self.set_progress_bar_fraction, i / 1000)
                img = self.normalize(image, float(d / 1000), y_size, x_size)
                rad_prof = self.radial_profile(img, [y_size / 2, x_size / 2])
                if type == "H-alpha":
                    center_prof = np.average(rad_prof[0:250])
                    edge_prof = np.median(rad_prof[(int(x_size / 2)) - 150:(int(x_size / 2)) - 45])
                if type == "CaII K":
                    center_prof = np.average(rad_prof[0:150])
                    edge_prof = np.average(rad_prof[(int(x_size / 2)) - 175:(int(x_size / 2)) - 75])
                if abs(center_prof - edge_prof) < best[1]:
                    best[0] = d
                    best[1] = abs(center_prof - edge_prof)
                img = None
            self.infoLabel.set_text("[!] Done. Coefficient was 0.{}".format(str(best[0])))
            print ("\n[!] Done. Coefficient was 0." + str(best[0]))
        else:
            best[0] = int(coef)
        image = self.normalize(image, float(best[0] / 1000), y_size, x_size)
        image = Image.fromarray(image.astype(np.uint16))
        image.save(output)
        self.doButton.set_sensitive(True)
        self.infoLabel.set_text("[!] Correction done.")
        print ("[!] Correction done.")
        self.thread._Thread__stop()


if __name__ == '__main__':
    window = Window()
    gtk.main()
    quit()
