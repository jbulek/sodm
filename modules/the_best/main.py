from __future__ import division
import gtk
import pygtk
import sys
import threading

pygtk.require('2.0')
gtk.gdk.threads_init()


class Window(gtk.Window):
    def __init__(self):
        builder = gtk.Builder()
        builder.add_from_file("modules/the_best/main.gui")
        builder.connect_signals(self)
        self.fileDialog = builder.get_object("fileDialog")
        self.fileChooser = builder.get_object('filechooser')
        self.fileTreeView = builder.get_object('fileTreeView')
        self.bestTreeView = builder.get_object('bestTreeView')
        self.selectButton = builder.get_object('selectButton')
        self.doButton = builder.get_object('doButton')
        self.progressBar = builder.get_object('progressbar')
        self.infoLabel = builder.get_object('infoLabel')
        self.file_store = gtk.ListStore(str)
        self.best_store = gtk.ListStore(int, str)
        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn("", renderer, text=0)
        self.fileTreeView.append_column(column)
        column = gtk.TreeViewColumn("#", renderer, text=0)
        column1 = gtk.TreeViewColumn("Soubor", renderer, text=1)
        self.bestTreeView.append_column(column)
        self.bestTreeView.append_column(column1)
        self.fileList = []

        window = builder.get_object("thebestWindow")
        window.show_all()

    def doButton_clicked_cb(self, *args):
        from threading import Thread
        from modules.utils import viewer
        coords=viewer.get_xy(self.fileList)
        bestList = Thread(target=self.findbest,args=[self.fileList, coords]).start()


    def selectButton_clicked_cb(self, *args):
        self.fileDialog.run()

    def fileDialogOKButton_clicked(self, *args):
        self.fileList = self.fileChooser.get_filenames()
        for file in self.fileList:
            self.file_store.append([file.split("\\")[-1]])
        self.fileTreeView.set_model(self.file_store)
        self.fileDialog.hide()

    def fileDialogCancelButton_clicked_cb(self, *args):
        self.fileDialog.hide()

    def onDeleteWindow(self, *args):
        gtk.main_quit(*args)
        return 0

    def set_progress_bar_fraction(self, fraction):
        self.progressBar.set_fraction(fraction)

    def findbest(self, r, coords):
        import shutil
        from modules.utils import tools
        import fnmatch
        import os
        import sys
        import cv2
        import numpy as np
        import gobject
        from PIL import Image
        self.doButton.set_sensitive(False)
        self.selectButton.set_sensitive(False)
        self.infoLabel.set_text("")
        self.infoLabel.set_text("[*] Preparing images")
        tools.convert(r, "tiff", coords)
        folder = r[0].split("\\")[:-1]
        folder = '/'.join(folder)
        folder += '/'
        print("[*] Looking for best image of series")
        self.infoLabel.set_text("[*] Looking for best image of series")
        data_inputs = fnmatch.filter(os.listdir(folder + '/tmp/'), '*.tiff')
        nplist = {}
        npedges = []
        for i in range(len(data_inputs)):
            gobject.idle_add(self.set_progress_bar_fraction, (i+1)/len(data_inputs))
            self.infoLabel.set_text("[*] Processing image {} of {}".format(i + 1, len(data_inputs)))
            sys.stdout.write("\r[*] Processing image %d of %d" % (i + 1, len(data_inputs)))
            im = Image.open(folder + '/tmp/' + data_inputs[i])
            array = np.asarray(im, dtype=np.uint16)
            gy, gx = np.gradient(array)
            gnorm = np.sqrt(gx ** 2 + gy ** 2)
            npedges.append(np.average(gnorm))
            nplist[data_inputs[i]] = npedges[i]
        min = np.min(npedges)
        for key, value in nplist.iteritems():
            nplist[key] = (value / min) * 100
        print list
        bestimg = (sorted(list, key=lambda i: int(nplist[i])))
        shutil.rmtree(folder + '/tmp')
        i = 1
        for file in bestimg:
            self.best_store.append([i, file])
            i += 1
        self.bestTreeView.set_model(self.best_store)
        self.doButton.set_sensitive(True)
        self.selectButton.set_sensitive(True)

if __name__ == '__main__':
    sys.setrecursionlimit(100000)
    threading.stack_size(200000000)
    window = Window()
    gtk.main()
    quit()
